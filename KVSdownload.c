/*(c)ema 19.2.2009, kvs download postprocessor V3*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc,char *argv[])
{
  FILE *fi,*fp;
  char s[250],radka[250],cmd[250],dir[250],dlname[250],*appdata,monax[250],garbage[250];
  int i,j,c,n,err;
  fprintf(stdout,"kvs download postprocessor V3\n");
  fflush(stdout);
  appdata=getenv("APPDATA");

return 9;

  if(argc!=2) {
    fprintf(stderr,"usage: %s file.kvsfmt \n",argv[0]);
    fflush(stderr);
    gets(s);
    return 1;
  }
  if((fi=fopen(argv[1],"rb"))==NULL) {
    fprintf(stderr,"can't open kvs file %s.\n",argv[1]);
    fflush(stderr);
    gets(s);
    return 1;
  }
  sprintf(monax,"%s\\monaX",appdata);
//fprintf(stdout,"num of arg: %i\n",argc);
//fprintf(stdout,"input:\n%s\n",argv[1]);
//fprintf(stdout,"monaX: %s\n",monax);
//fflush(stdout);
  if((fp=fopen(monax,"r"))==NULL) {
    fprintf(stderr,"can't open %s file.\n",monax);
    fflush(stderr);
    fclose(fi);
    gets(s);
    return 1;
  }
/*************************** sel output dir *************************/
  i=1;
  n=0;
  fprintf(stderr,"select output directory:\n");
  fflush(stderr);
  while(!feof(fp)) {
    fgets(radka,245,fp);
    if(!feof(fp))fprintf(stdout,"%3i %s",i++,radka);
    if(!feof(fp))fflush(stdout);
  }
  fclose(fp);
  while(n<1 || n>=i){
    fprintf(stdout,"key num: ");
    fflush(stdout);
    scanf("%i",&n);
    if(n<1 || n>=i)fprintf(stdout,"invalid num.\n");
    if(n<1 || n>=i)fflush(stdout);
  }
  sprintf(monax,"%s/monaX",appdata);
  fp=fopen(monax,"r");
  i=1;
  while(!feof(fp)) {
    fgets(radka,245,fp);
    sscanf(radka,"%s%s",dlname,dir);
    if(i == n)break;
    i++;
  }
  fclose(fp);
//fprintf(stdout,"%i %i %s %s\n",i,n,dlname,dir);
//fflush(stdout);
/*************************** tar file *******************************/
  fprintf(stdout,"extract file(s) ...\n");
  fflush(stdout);
  sprintf(cmd,"tar -tf \"%s\"",argv[1]);
  fp=popen(cmd,"r");
  fgets(garbage,245,fp);
  garbage[strlen(garbage)-1]='\0';
  pclose(fp);
  sprintf(cmd,"tar -xvf \"%s\" -C %s \"*\"",argv[1],dir);
//fprintf(stdout,"cmd1: %s\n",cmd);
  err=system(cmd);
  if(err != 0){
    fprintf(stderr,"can't extract file(s) from tar archiv:\n%s",argv[1]);
    fflush(stderr);
    gets(s);
    gets(s);
    return 1;
  }
  sprintf(cmd,"rm \"%s/%s\"",dir,garbage);
//fprintf(stdout,"cmd2: %s\n",cmd);
  system(cmd);
  return 0;
/*************************** default ********************************/
  sprintf(cmd,"cp \"%s\" \"%s\"",argv[1],dir);
  err=system(cmd);
  if(err != 0){
    fprintf(stderr,"can't copy file:\n%s\nto:\n%s",argv[1],dir);
    fflush(stderr);
    gets(s);
    gets(s);
    return 1;
  }
  return 0;
}
